<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  


class Itoris_ProductPriceVisibility_Helper_Data extends Mage_Core_Helper_Abstract {

	protected $alias = 'product_visibility';
	protected $visibilityProductCollection = null;
	protected $categorySettings = null;
	protected $settingsModel = null;
	protected $categoryRuleResults = array();
	protected $categoryGroupIds = array();
    private $_isRegisteredFrontend = null;

    public function isAdminRegistered() {
        try {
            return true;
        } catch(Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return false;
        }
    }

	public function isRegisteredAutonomous($website = null) {
      	return true;
	}

	public function registerCurrentStoreHost($sn) {
		return true;
	}

	public function isRegistered($website) {
		return true;
	}

	public function getAlias() {
		return $this->alias;
	}

	/**
	 * Get store id by parameter from the request
	 *
	 * @return int
	 */
	public function getStoreId() {
		if (Mage::app()->getRequest()->getParam('store')) {
			return Mage::app()->getStore(Mage::app()->getRequest()->getParam('store'))->getId();
		}
		return 0;
	}

	/**
	 * Get website id by parameter from the request
	 *
	 * @return int
	 */
	public function getWebsiteId() {
		if (Mage::app()->getRequest()->getParam('website')) {
			return Mage::app()->getWebsite(Mage::app()->getRequest()->getParam('website'))->getId();
		}
		return 0;
	}

	/**
	 * Get settings
	 *
	 * @return Itoris_ProductPriceVisibility_Model_Settings
	 */
	public function getSettings($backend = false, $productId = null) {
		/** @var $settingsModel Itoris_ProductPriceVisibility_Model_Settings */
		$settingsModel = Mage::getSingleton('itoris_productpricevisibility/settings');
		if (is_null($productId)) {
			$productId = 0;
		}
		if (!$productId && $this->settingsModel) {
			return $this->settingsModel;
		}
		if ($backend || !Mage::app()->getWebsite()->getId()) {
			$settingsModel->load($this->getWebsiteId(), $this->getStoreId(), $productId);
		} else {
			$settingsModel->load(Mage::app()->getWebsite()->getId(), Mage::app()->getStore()->getId(), $productId);
		}
		if (!$productId) {
			$this->settingsModel = $settingsModel;
		}

		return $settingsModel;
	}

	public function getScopeData() {
		if ($this->getStoreId()) {
			return array(
				'scope'    => 'store',
				'scope_id' => $this->getStoreId(),
			);
		} elseif ($this->getWebsiteId()) {
			return array(
				'scope'    => 'website',
				'scope_id' => $this->getWebsiteId(),
			);
		} else {
			return array(
				'scope'    => 'default',
				'scope_id' => 0
			);
		}
	}

	public function getDate($dateOrigValue) {
		$dateOrig = new Zend_Date($dateOrigValue, Zend_Date::ISO_8601);
		$dateWithTimezone = new Zend_Date($dateOrig, Zend_Date::ISO_8601);
		$currentTimezone = Mage::app()->getLocale()->date()->getTimezone();
		if ($dateWithTimezone->getTimezone() != $currentTimezone) {
			$dateWithTimezone->setTimezone(Mage::app()->getLocale()->date()->getTimezone());
            $dateWithTimezone->setHour($dateOrig->getHour());
            $dateWithTimezone->setDay($dateOrig->getDay());
            $dateWithTimezone->setMonth($dateOrig->getMonth());
            $dateWithTimezone->setYear($dateOrig->getYear());
		}

		return $dateWithTimezone;
	}

    public function customerGroup($selectedGroupId) {
        $customer = Mage::getSingleton('customer/session');
        $allowedGroups = array();
		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
			$_customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
			$customer = Mage::getModel("customer/customer")->load($_customerId);
			$customerId = $customer->getGroupId();
		} else $customerId = 0;
        //$customerId = $customer->getCustomerGroupId();
        if (is_array($selectedGroupId)) {
            foreach ($selectedGroupId as $key => $value) {
                if ($value['group_id'] !== null) {
                    $allowedGroups[] = $value['group_id'];
                }
            }
        } else {
            $allowedGroups = explode(',', $selectedGroupId);
        }
        $allowedGroups = array_map('intval', $allowedGroups);
        if (is_null($selectedGroupId)) {
            return false;
        } else {
            if (in_array($customerId, $allowedGroups)) {
                return true;
            } else {
                return false;
            }
        }
    }

	public function isVisibleByRestrictionDate($startDate, $endDate) {
        try {
            $currentDate = Mage::app()->getLocale()->date();
            $start = $startDate == ''? null : $this->getDate($startDate);
            $end = $endDate == '' ? null : $this->getDate($endDate);
            if (!empty($startDate) && !empty($endDate)) {
                if ($start->compareDate($currentDate) !== 1 && $end->compareDate($currentDate) !== -1) {
                    return true;
                } else {
                    return false;
                }
            } elseif (!empty($startDate) && empty($endDate)) {
                if ($start->compareDate($currentDate) !== 1) {
                    return true;
                } else {
                    return false;
                }
            } elseif (empty($startDate) && !empty($endDate))  {
                if ($end->compareDate($currentDate) !== -1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } catch(Exception $e) {
            Mage::logException($e);
        }
        return false;
	}

	public function isRegisteredFrontend() {
        if ($this->_isRegisteredFrontend === null) {
            $this->_isRegisteredFrontend = !Mage::app()->getStore()->isAdmin()
			                                && $this->getSettings()->getEnabled()
			                                && $this->isRegisteredAutonomous();
        }

        return $this->_isRegisteredFrontend;
	}

	public function isRightConditions($model, $groupId, $mode = 'product') {
		if ($mode == 'product') {
			if ($this->customerGroup($groupId)
				&& $this->isVisibleByRestrictionDate($model->getProductRestrictionBegin(), $model->getProductRestrictionEnd())
			) {
				return true;
			}
		} elseif ($mode == 'price') {
			if ($this->customerGroup($groupId)
				&& $this->isVisibleByRestrictionDate($model->getPriceRestrictionBegin(), $model->getPriceRestrictionEnd())
			) {
				return true;
			}
		} elseif ($mode == 'category') {
			if ($this->customerGroup($groupId)
				&& $this->isVisibleByRestrictionDate($model->getCategoryRestrictionBegin(), $model->getCategoryRestrictionEnd())
			) {
				return true;
			}
		}

	}

	public function getGroupId($tableName, $prodId, $checkStore) {
		$productId = (int)$prodId;
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		$checkStore = intval($checkStore);
		if ($this->useDefault($tableName, $productId, $checkStore)) {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where product_id={$productId} and store_id=0");
		} else {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where product_id={$productId} and store_id={$checkStore}");
		}
		return $groupIdByArray;
	}

	public function getGroupIdsProducts($tableName, $productIds, $storeId) {
		return $this->_getGroupIdsByType($tableName, $productIds, 'product_id', $storeId);
	}

	protected function _getGroupIdsByType($tableName, $typeIds, $type, $storeId) {
		if (!is_array($typeIds)) {
			$typeIds = array($typeIds);
		}
		$typeIds = array_map('intval', $typeIds);
		if (!($type == 'product_id' || $type == 'category_id')) {
			return array();
		}

		if (empty($typeIds)) {
			$whereCondition = '';
		} else {
			$typeIds = implode(',', $typeIds);
			$whereCondition = "{$type} in ({$typeIds}) and";
		}
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		$storeId = intval($storeId);
		$result = $connection->fetchAll("select group_id,store_id,{$type} from {$groupTable} where {$whereCondition} (store_id=0 or store_id={$storeId}) order by store_id asc");
		$groupsByTypeIdDefault = array();
        $groupsByTypeId = array();
        foreach ($result as $row) {
            if ($row['store_id']) {
                if (!isset($groupsByTypeId[$row[$type]])) {
                    $groupsByTypeId[$row[$type]] = array();
                }
                $groupsByTypeId[$row[$type]][] = array('group_id' => $row['group_id']);
            } else {
                if (!isset($groupsByTypeIdDefault[$row[$type]])) {
                    $groupsByTypeIdDefault[$row[$type]] = array();
                }
                $groupsByTypeIdDefault[$row[$type]][] = array('group_id' => $row['group_id']);
            }
		}

        foreach ($groupsByTypeId as $typeId => $groups) {
            $groupsByTypeIdDefault[$typeId] = $groups;
        }
		return $groupsByTypeIdDefault;
	}

	public function getGroupIdForCategory($tableName, $catId, $checkStore) {
		$categoryId = (int)$catId;
		$checkStore = intval($checkStore);
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		if ($this->useDefaultForCategory($tableName, $categoryId, $checkStore)) {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where category_id={$categoryId} and store_id=0");
		} else {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where category_id={$categoryId} and store_id={$checkStore}");
		}
		return $groupIdByArray;
	}

	public function useDefault($tableName, $productId, $checkStore) {
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		$storeIds = $connection->fetchAll("select store_id from {$groupTable} where product_id={$productId}");
		$storeId = array();
		foreach ($storeIds as $value) {
			$storeId[] = $value['store_id'];
		}
		if (in_array($checkStore, $storeId)) {
			return false;
		} else {
			return true;
		}
	}

	public function useDefaultForCategory($tableName, $categoryId, $checkStore) {
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		$storeIds = $connection->fetchAll("select store_id from {$groupTable} where category_id={$categoryId}");
		$storeId = array();
		foreach ($storeIds as $value) {
			$storeId[] = $value['store_id'];
		}
		if (in_array($checkStore, $storeId)) {
			return false;
		} else {
			return true;
		}
	}

	public function rulesForCategory($categoryIds, $checkStore, $mode) {
		if (!empty($categoryIds)) {
			foreach ($categoryIds as $catId) {
				$ruleIndex = $mode . '_cat' . $catId . '_s' . $checkStore;
				if (array_key_exists($ruleIndex, $this->categoryRuleResults)) {
					if ($this->categoryRuleResults[$ruleIndex]) {
						return $this->categoryRuleResults[$ruleIndex];
					} else {
						continue;
					}
				}
				$categoryId = (int)$catId;
				$categoryModel = $this->getCategorySettings($categoryId);
				$categoryMode = (int)$categoryModel->getCategoryHidingMode();
				$categoryGroupId = $this->getGroupIdCategory($categoryId, 'itoris_productpricevisibility_category_visibility_group', $checkStore);
				if ($this->isRightConditions($categoryModel, $categoryGroupId, 'category')) {
					if ($mode == 'hide') {
						if ($categoryMode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::HIDE_COMPLETELY) {
							$this->categoryRuleResults[$ruleIndex] = true;
							return true;
						}
					} elseif ($mode == 'out_of_stock') {
						if ($categoryMode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::SHOW_OUT_OF_STOCK) {
							$this->categoryRuleResults[$ruleIndex] = true;
							return true;
						}
					} elseif($mode == 'price') {
						$modeCategory = array();
						if ($categoryMode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::NO_PRICE) {
							$modeCategory[] = 'itoris_price';
						} elseif ($categoryMode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::CUSTOM_MESSAGE) {
							$modeCategory[] = 'custom_message';
							$modeCategory[] = $categoryModel->getCategoryHidingMessage();
						} elseif ($categoryMode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
							$modeCategory[] = 'restriction_message';
							$modeCategory[] = $categoryModel->getCategoryRestrictionMessage();
						}
						$this->categoryRuleResults[$ruleIndex] = $modeCategory;
						return $modeCategory;
					}
				}
				$this->categoryRuleResults[$ruleIndex] = null;
			}
		}
		return null;
	}

	protected function getGroupIdCategory($categoryId, $tableName, $storeId) {
		$index = $tableName . '_s' . $storeId;
		if (!array_key_exists($index, $this->categoryGroupIds)) {
			$this->categoryGroupIds[$index] = $this->_getGroupIdsByType($tableName, array(), 'category_id', $storeId);
		}
		if (isset($this->categoryGroupIds[$index][$categoryId])) {
			return $this->categoryGroupIds[$index][$categoryId];
		}
		return null;
	}

	public function getCategorySettings($categoryId) {
		if (is_null($this->categorySettings)) {
			$this->categorySettings = Mage::getModel('itoris_productpricevisibility/settings')->loadSettingsForCategory(Mage::app()->getWebsite()->getId(), Mage::app()->getStore()->getId());
		}
		$settings = new Varien_Object();
		if (isset($this->categorySettings[$categoryId])) {
			$settings->addData($this->categorySettings[$categoryId]);
		}
		return $settings;
	}

	protected function getVisibilityProductCollection() {
		if (is_null($this->visibilityProductCollection)) {
			return $this->visibilityProductCollection = Mage::getModel('itoris_productpricevisibility/settings');
		} else {
			return $this->visibilityProductCollection;
		}
	}
}
?>