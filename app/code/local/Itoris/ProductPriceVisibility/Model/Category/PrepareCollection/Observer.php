<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Category_PrepareCollection_Observer {

	public function beforeLoad($obj) {
		$categoryCollection = $obj->getCategoryCollection();
		if($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = Mage::app()->getStore()->getId();
			$categoryVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
			$settingsByCategoryId = $categoryVisibilityModel->loadSettingsForCategory(0, $checkStore);
            $globalSettings = $categoryVisibilityModel->load(Mage::app()->getWebsite()->getId(), Mage::app()->getStore()->getId());
            $globalGroupId = $globalSettings->getGlobalUserGroups();
            $globalAllHide = ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL
                && $this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
            );
            $idCategoryVisible = array();
            foreach ($settingsByCategoryId as $categoryId => $category) {
                $category = new Varien_Object($category);
                $categoryGroupId = $this->getDataHelper()->getGroupIdForCategory('itoris_productpricevisibility_category_visibility_group', (int)$categoryId, $checkStore);
                if (((int)$category->getData('category_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::HIDE_COMPLETELY
                        || (int)$category->getData('category_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::HIDE_ONLY_CATEGORY)
                    && $this->getDataHelper()->customerGroup($categoryGroupId)
                    && $this->getDataHelper()->isVisibleByRestrictionDate($category->getData('category_restriction_begin'), $category->getData('category_restriction_end'))
                ) {
                    $categoryIdHide[] = $categoryId;
                } elseif (($this->getDataHelper()->customerGroup($categoryGroupId)
                        && $this->getDataHelper()->isVisibleByRestrictionDate($category->getData('category_restriction_begin'), $category->getData('category_restriction_end')))
                ) {
                    $idCategoryVisible[] = $categoryId;

                }
            }
            if ($globalAllHide) {
                $categoryIdHide = $categoryCollection->getAllIds();
                if (!empty($idCategoryVisible)) {
                    foreach ($idCategoryVisible as $id) {
                        if ($key = array_search($id, $categoryIdHide)) {
                            unset($categoryIdHide[$key]);
                        }
                    }
                }
            }
			if (!empty($categoryIdHide)) {
				$categoryCollection->addFieldToFilter('entity_id', array('nin' => $categoryIdHide));
			}
		}
	}

	public function currentCategory($obj) {
		$category = $obj->getCategory();
		if($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = (int)Mage::app()->getStore()->getId();
			$categoryVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
			$categoryVisibility = $categoryVisibilityModel->load(0, $checkStore, 0, $category->getId());
			$categoryGroupId = $this->getDataHelper()->getGroupIdForCategory('itoris_productpricevisibility_category_visibility_group', $category->getId(), $checkStore);
			if (($categoryVisibility->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::HIDE_COMPLETELY
                || $categoryVisibility->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::HIDE_ONLY_CATEGORY)
				&& $this->getDataHelper()->customerGroup($categoryGroupId)
				&& $this->getDataHelper()->isVisibleByRestrictionDate($categoryVisibility->getCategoryRestrictionBegin(), $categoryVisibility->getCategoryRestrictionEnd())
			) {
                $redirectUrl = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                Mage::app()->getResponse()->setRedirect("$redirectUrl", 404);
                Mage::register('is_current_category', true);
			} elseif ($categoryVisibility->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::REDIRECT
					&& $this->getDataHelper()->customerGroup($categoryGroupId)
					&& $this->getDataHelper()->isVisibleByRestrictionDate($categoryVisibility->getCategoryRestrictionBegin(), $categoryVisibility->getCategoryRestrictionEnd())
			) {
				$redirectUrl = $categoryVisibility->getCategoryRedirect();
				Mage::app()->getResponse()->setRedirect("$redirectUrl", 301);
                Mage::register('is_current_category', true);
                return;
			}
            $globalSettings = $categoryVisibilityModel->load(Mage::app()->getWebsite()->getId(), $checkStore);
            $globalGroupId = $globalSettings->getGlobalUserGroups();
            $globalAllHide = ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL
                && $this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
            );
            if ($globalAllHide
            && !( $this->getDataHelper()->customerGroup($categoryGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($categoryVisibility->getCategoryRestrictionBegin(), $categoryVisibility->getCategoryRestrictionEnd())
                )
            ) {
                $redirectUrl = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                Mage::app()->getResponse()->setRedirect("$redirectUrl", 404);
            }
		}
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}
}
?>