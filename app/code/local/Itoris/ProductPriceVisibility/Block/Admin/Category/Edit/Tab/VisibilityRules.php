<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules extends Mage_Adminhtml_Block_Widget_Form {

	const HIDE_COMPLETELY = 1;
	const SHOW_OUT_OF_STOCK = 2;
	const NO_PRICE = 3;
	const CUSTOM_MESSAGE = 4;
	const REDIRECT = 5;
	const SHOW_PRICE_DISALLOW_ADD_TO_CART = 6;
    const HIDE_ONLY_CATEGORY = 7;

	protected function _prepareLayout() {
		parent::_prepareLayout();
		Varien_Data_Form::setFieldsetElementRenderer(
			$this->getLayout()->createBlock('itoris_productpricevisibility/admin_form_renderer_product_element')
		);

		return $this;
	}

	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$checkStore = (int)Mage::app()->getRequest()->getParam('store');
		$categoryId = (int)Mage::registry('current_category')->getId();
		$categorySetting = Mage::getModel('itoris_productpricevisibility/settings')->load(0, $checkStore, 0, $categoryId);
		$groupIdValue = $this->getGroupId('itoris_productpricevisibility_category_visibility_group', $categoryId, $checkStore);

		$visibilityRules = $form->addFieldset('category_visibility', array(
			'legend' => $this->__('Visibility Rules'),
		));

		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
		$visibilityRules->addField('category_user_groups', 'multiselect', array(
			'name'   => 'itoris_category_visibility[groups]',
			'label'  => $this->__('Category is hidden to the following user groups (multi-select):'),
			'title'  => $this->__('Category is hidden to the following user groups (multi-select):'),
			'values' => $userGroups,
			'note'  => $this->__('CTRL + click to select/deselect group'),
			'value'  => $groupIdValue,
			'show_checkbox' => $this->getDataHelper()->useDefaultForCategory('itoris_productpricevisibility_category_visibility_group', $categoryId, $checkStore)
		));

		$visibilityRules->addField('category_restriction_begin', 'date', array(
			'name'		   => 'itoris_category_visibility[restriction_begin]',
			'label'        => $this->__('Restriction by group begins on'),
			'title'        => $this->__('Restriction by group begins on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $categorySetting->getCategoryRestrictionBegin(),
			'show_checkbox' => $categorySetting->isParentValue('category_restriction_begin'),
            'class'        => 'validate-date-one-type'
		));

		$visibilityRules->addField('category_restriction_end', 'date', array(
			'name'		   => 'itoris_category_visibility[restriction_end]',
			'label'        => $this->__('Restriction by group ends on'),
			'title'        => $this->__('Restriction by group ends on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $categorySetting->getCategoryRestrictionEnd(),
			'show_checkbox' => $categorySetting->isParentValue('category_restriction_end'),
            'class'        => 'validate-date-one-type'
		));
		$hidingMode = $categorySetting->getCategoryHidingMode();
		$visibilityRules->addField('category_hiding_mode', 'select', array(
			'name'		  => 'itoris_category_visibility[hiding_mode]',
			'label'       => $this->__('Category hiding mode:'),
			'title'       => $this->__('Category hiding mode:'),
			'show_checkbox' => $categorySetting->isParentValue('category_hiding_mode'),
			'values' => array(
				array(
					'label' => $this->__('Hide Category and All Products in it'),
					'value' => self::HIDE_COMPLETELY,
				),
                array(
					'label' => $this->__('Hide only Category, leave Products visible'),
					'value' => self::HIDE_ONLY_CATEGORY,
				),
				array(
					'label' => $this->__('Show "Out of Stock" to All Products'),
					'value' => self::SHOW_OUT_OF_STOCK,
				),
				array(
					'label' => $this->__('No Price to All Products'),
					'value' => self::NO_PRICE,
				),
				array(
					'label' => $this->__('Custom Message for All Products'),
					'value' => self::CUSTOM_MESSAGE,
				),
				array(
					'label' => $this->__('Redirect'),
					'value' => self::REDIRECT,
				),
				array(
					'label' => $this->__('Show Product Prices, disallow adding to Cart'),
					'value' => self::SHOW_PRICE_DISALLOW_ADD_TO_CART,
				),
			),
			'value'      => $hidingMode,
			'onchange'    => "
				if (this.value == 4) {
					$('itoris_hiding_message_box').show();
					$('itoris_redirect_box').hide();
					$('itoris_restriction_message_box').hide();
					$('itoris_redirect').disabled = true;
					$('itoris_hiding_message').disabled = false;
				} else if (this.value == 5) {
					$('itoris_redirect_box').show();
					$('itoris_hiding_message_box').hide();
					$('itoris_restriction_message_box').hide();
					$('itoris_hiding_message').removeClassName('required-entry');
					$('itoris_redirect').disabled = false;
					$('itoris_hiding_message').disabled = true;
				} else if (this.value == 6) {
					$('itoris_restriction_message_box').show();
					$('itoris_hiding_message_box').hide();
					$('itoris_redirect_box').hide();
				} else {
					$('itoris_hiding_message').removeClassName('required-entry');
					$('itoris_redirect').removeClassName('required-entry');
					$('itoris_hiding_message').disabled = true;
					$('itoris_redirect').disabled = true;
					$('itoris_redirect_box').hide();
					$('itoris_hiding_message_box').hide();
					$('itoris_restriction_message_box').hide();
				}
			"
		));

		$isVisibleCustomMessage = $hidingMode == self::CUSTOM_MESSAGE ? true : false;
		$disabledCustomMessage = $isVisibleCustomMessage ? false : true;
		$elementRenderer = new Itoris_ProductPriceVisibility_Block_Admin_Form_Renderer_Element();
		$visibilityRules->addField('itoris_hiding_message', 'text', array(
			'name'	       => 'itoris_category_visibility[hiding_message]',
			'label'        => $this->__('Custom Message:'),
			'title'        => $this->__('Custom Message:'),
			'required'     => true,
			'disabled'	   => $disabledCustomMessage,
			'value'        => $categorySetting->getCategoryHidingMessage(),
			'container_id' => 'itoris_hiding_message_box',
			'show_checkbox' => $categorySetting->isParentValue('category_hiding_message'),
			'is_visible'   => $isVisibleCustomMessage
		))->setRenderer($elementRenderer);

		$isVisibleRedirect = $hidingMode == self::REDIRECT ? true : false;
		$disabledRedirect = $isVisibleRedirect ? false : true;
		$visibilityRules->addField('itoris_redirect', 'text', array(
			'name'	       => 'itoris_category_visibility[redirect]',
			'label'        => $this->__('Redirect URL:'),
			'title'        => $this->__('Redirect URL:'),
			'required'     => true,
			'disabled'	   => $disabledRedirect,
			'value'        => $categorySetting->getCategoryRedirect(),
			'show_checkbox' => $categorySetting->isParentValue('category_redirect'),
			'container_id' => 'itoris_redirect_box',
			'is_visible'   => $isVisibleRedirect
		))->setRenderer($elementRenderer);

		$isVisibleRestrictionMessage = $hidingMode == self::SHOW_PRICE_DISALLOW_ADD_TO_CART ? true : false;
		$visibilityRules->addField('itoris_restriction_message', 'text', array(
			'name'	       => 'itoris_category_visibility[restriction_message]',
			'label'        => $this->__('Restriction Message:'),
			'title'        => $this->__('Restriction Message:'),
			'value'        => $categorySetting->getCategoryRestrictionMessage(),
			'show_checkbox' => $categorySetting->isParentValue('category_restriction_message'),
			'container_id' => 'itoris_restriction_message_box',
			'is_visible'   => $isVisibleRestrictionMessage
		))->setRenderer($elementRenderer);
		$this->setForm($form);
	}

	protected function getGroupId($tableName, $categoryId, $checkStore) {
		$groupIdByArray = $this->getDataHelper()->getGroupIdForCategory($tableName, $categoryId, $checkStore);
		$groupId = '';
		foreach ($groupIdByArray as $key => $value) {
			if ($key == count($groupIdByArray) - 1) {
				$groupId .= $value['group_id'];
			} else {
				$groupId .= $value['group_id'] . ', ';
			}
		}
		return $groupId;
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}
}
?>