<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */
/**/

class Itoris_PendingRegistration_Block_Admin_Container extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct() {
		parent::__construct();
		
		$this->_blockGroup = 'itoris_pendingregistration';
		$this->_controller = 'admin'; 
		$this->_mode = '';
		
        $this->_controller = 'container';
        $this->_updateButton( 'save', 'label', $this->__('Save Template') );
        $this->_removeButton( 'delete' );
        $this->_removeButton( 'back' );
        $this->_removeButton( 'reset' );
	}
	
	public function getHeaderText() {
		
		return $this->__('Template settings')." (".$this->__($this->getEmailTemplate()->getTitleExt()).")";
	}
	
	public function _prepareLayout()
	{
		$this->setChild( 'form', $this->getLayout()->createBlock( 'itoris_pendingregistration/admin_form' ) );
		parent::_prepareLayout();
		
	}

	/**
	 * @param Itoris_PendingRegistration_Model_Template $template
	 * @return void
	 */
	public function setEmailTemplate(Itoris_PendingRegistration_Model_Template $template){
		$this->setData('email_template', $template);
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Template
	 */
	public function getEmailTemplate(){
		return $this->getData('email_template');
	}

	public function setScope(Itoris_PendingRegistration_Model_Scope $scope){
		return $this->setData('scope', $scope);
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getScope(){
		return $this->getData('scope');
	}
}
?>