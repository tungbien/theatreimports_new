<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Admin_CustomerGroups extends Mage_Adminhtml_Block_Template {

	protected $isUsedParentFlag = false;

	protected function _construct() {
		$this->setTemplate('itoris/pendingregistration/customer_groups.phtml');
	}

	public function getCustomerGroups() {
		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
		foreach ($userGroups as $key => $value) {
			if (!$value['value']) {
				unset($userGroups[$key]);
			}
		}
		array_unshift($userGroups, array(
			'label' => $this->__('All Groups'),
			'value' => '',
		));
		return $userGroups;
	}

	public function getSelectedGroups() {
		$groupModel = Mage::getModel('itoris_pendingregistration/customerGroup');
		$groups = $groupModel->getGroups($this->getWebsiteId(), $this->getStoreId());
		$this->isUsedParentFlag = $groupModel->isUsedParentValue();
		return $groups;
	}

	public function getWebsiteId() {
		return Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getId();
	}

	public function getStoreId() {
		return Mage::app()->getStore($this->getRequest()->getParam('store'))->getId();
	}

	public function isUsedParent() {
		return $this->isUsedParentFlag;
	}
}

?>