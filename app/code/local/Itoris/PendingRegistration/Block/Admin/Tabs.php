<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Admin_Tabs extends Mage_Adminhtml_Block_System_Config_Tabs
{
	protected function _construct()
	{
		parent::_construct();
		$this->setTitle( '' );
		$this->setTemplate( 'itoris/pendingregistration/tabs.phtml' );
	}

	public function initTabs()
	{	
		
		$this->addTab( 'special', array(
			'label'     => $this->__('Events')
		));

		/** @var $url Mage_Adminhtml_Model_Url */
		$url = Mage::getModel('adminhtml/url');
		
		if( Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($this->getScope()) )
		{
			/** @var $template Itoris_PendingRegistration_Model_Template */
			$template = Mage::getModel('itoris_pendingregistration/template');
			$urlParams = $this->getScope()->getSummaryForUrl();

			$template->load(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN, 'type', $this->getScope());
			$urlParams['template'] = Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN;
			$this->addSection( 'template1', 'special', array(
					'label'     => $this->__('Email to admin'),
					'url'		=> $url->getUrl( '*/*/index', $urlParams),
					'active'	=> $template->isActive()
			));

			$template->load(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_USER, 'type', $this->getScope());
			$urlParams['template'] = Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_USER;
			$this->addSection( 'template2', 'special', array(
					'label'     => $this->__('Email to user'),
					'url'		=> $url->getUrl( '*/*/index', $urlParams),
					'active'	=> $template->isActive()
			));

			$template->load(Itoris_PendingRegistration_Model_Template::$EMAIL_APPROVED, 'type', $this->getScope());
			$urlParams['template'] = Itoris_PendingRegistration_Model_Template::$EMAIL_APPROVED;
			$this->addSection( 'template3', 'special', array(
					'label'     => $this->__('Email approved'),
					'url'		=> $url->getUrl( '*/*/index', $urlParams ),
					'active'	=> $template->isActive()
			));

			$template->load(Itoris_PendingRegistration_Model_Template::$EMAIL_DECLAINED, 'type', $this->getScope());
			$urlParams['template'] = Itoris_PendingRegistration_Model_Template::$EMAIL_DECLAINED;
			$this->addSection( 'template4', 'special', array(
					'label'     => $this->__('Email declined'),
					'url'		=> $url->getUrl( '*/*/index', $urlParams ),
					'active'	=> $template->isActive()
			));

			
			$this->addTab( 'actions', array(
				'label'     => $this->__('Actions')
			));
			
			$this->addSection( 'action1', 'actions', array(
				'label'     => $this->__('Existing users'),
				'url'		=> $url->getUrl( '*/*/existen', array( 'action'=>1 ) ),
				'active'	=> null
			));

			$this->addSection('customer_groups', 'actions', array(
				'label'     => $this->__('Customer Groups'),
				'url'		=> $url->getUrl( '*/*/customergroups'),
				'active'	=> null
			));

			if ($this->getActiveTemplateType()) {
				$this->setActiveSection( 'template'.$this->getActiveTemplateType());
			}else{
				$this->setActiveSection( 'action1' );
			}
		}
		else
		{
			$this->addSection( 'template4', 'special', array(
					'label'     => $this->__("Please activate engine!"),
					'url'		=> $url->getUrl( '*/*/index' ),
					'active'	=> false
			));
		}
		
		return $this;
	}

	public function setScope(Itoris_PendingRegistration_Model_Scope $scope){
		return $this->setData('scope', $scope);
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getScope(){
		return $this->getData('scope');
	}

	public function getActiveTemplateType(){
		return $this->getData('active_template_type');
	}

	public function setActiveTemplateType($type){
		$this->setData('active_template_type', $type);
	}
}

?>