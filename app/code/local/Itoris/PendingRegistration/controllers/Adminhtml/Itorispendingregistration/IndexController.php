<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Adminhtml_Itorispendingregistration_IndexController extends Mage_Adminhtml_Controller_Action {
	
	private $currAction = 'index';
	
	/** @var Itoris_PendingRegistration_Model_Scope */
	private $scope = null;
	
	public function _construct() {
		parent::_construct();
		$this->getDataHelper()->init($this->getRequest());
	}

	public function preDispatch(){

		/** @var $scope Itoris_PendingRegistration_Model_Scope */

		$preDispatch = parent::preDispatch();

		$this->getDataHelper()->tryRegister();
		if(!$this->getDataHelper()->isAdminRegistered()) {
			$this->setFlag('', self::FLAG_NO_DISPATCH, true);
			Mage::getSingleton('adminhtml/session')->addError( 'Component not registered!' );
			$this->loadLayout();
			$register = $this->getLayout()->createBlock( 'itoris_pendingregistration/admin_register' );
			$this->getLayout()->getBlock( 'content' )->append( $register );
			$this->renderLayout();
		}

		/** @var $scope Itoris_PendingRegistration_Model_Scope */
		$scope = Mage::getModel('itoris_pendingregistration/scope');
		$scope->setStoreCode($this->getRequest()->getParam(Itoris_PendingRegistration_Model_Scope::$CONFIGURATION_SCOPE_STORE));
		$scope->setWebsiteCode($this->getRequest()->getparam(Itoris_PendingRegistration_Model_Scope::$CONFIGURATION_SCOPE_WEBSITE));
		$this->scope = $scope;

		return $preDispatch;
	}

	public function indexAction(){
		
		$this->loadLayout();

		$switcher = $this->getLayout()->createBlock('itoris_pendingregistration/admin_storeSwitcher');
		$this->getLayout()->getBlock('left')->append($switcher);

		/** @var $button Itoris_PendingRegistration_Block_Admin_Dropdown */
		$button = $this->getLayout()->createBlock( 'itoris_pendingregistration/admin_dropdown' );
		$button->setScope($this->scope);
		/** @var $scopeToggle Itoris_PendingRegistration_Block_Admin_ScopeToggle_EngineState */
		$scopeToggle = $this->getLayout()->createBlock('itoris_pendingregistration/admin_scopeToggle_engineState', 'scopeToggle');
		$scopeToggle->setScope($this->scope);

		/** @var $sett Itoris_PendingRegistration_Model_Settings */
		$sett = Mage::getModel('itoris_pendingregistration/settings');
		$scopeToggle->setActive(!$sett->recordExists('active', 'name', $this->scope->getTightScope()));
		$button->append($scopeToggle);
		$this->getLayout()->getBlock('left')->append( $button );

		/** @var $tabs Itoris_PendingRegistration_Block_Admin_Tabs */
		$tabs = $this->getLayout()->createBlock( 'itoris_pendingregistration/admin_tabs' );
		$tabs->setScope($this->scope);

		$templateType = $this->getRequest()->getParam('template', Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN);
		$tabs->setActiveTemplateType($templateType);
		$tabs->initTabs();
		$this->getLayout()->getBlock('left')->append($tabs);
		
		if (Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($this->scope)) {
		
			$text = $this->getLayout()->createBlock('itoris_pendingregistration/admin_magentoVariables');
			$this->getLayout()->getBlock('content')->append($text);

			/** @var $form Itoris_PendingRegistration_Block_Admin_Container */
			$form = $this->getLayout()->createBlock( 'itoris_pendingregistration/admin_container' );
			$form->setScope($this->scope);
			$this->getLayout()->getBlock( 'content' )->append( $form );
			/** @var $scopeToggleTemplate Itoris_PendingRegistration_Block_Admin_ScopeToggle_TemplateFieldset */
			$scopeToggleTemplate = $this->getLayout()->createBlock('itoris_pendingregistration/admin_scopeToggle_templateFieldset', 'scopeToggleTemplate');
			$scopeToggleTemplate->setScope($this->scope);
			$form->getChild('form')->append($scopeToggleTemplate);

			/** @var $template Itoris_PendingRegistration_Model_Template */
			$template = Mage::getModel('itoris_pendingregistration/template');

			$template->load($templateType, 'type', $this->scope);
			$template->setType($templateType);
			$form->setEmailTemplate($template);

			$scopeToggleTemplate->setActive(!$template->recordExists($templateType, 'type', $this->scope->getTightScope()));

			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		}
		
		$this->renderLayout();
	}

	public function saveAction() {
		$request = $this->getRequest();
		/** @var $template Itoris_PendingRegistration_Model_Template */
		$template = Mage::getModel('itoris_pendingregistration/template');
		$templateType = $this->getRequest()->getParam('template');
		if($templateType === null){
			throw new Exception($this->__("Unknown template type."));
		}
		$tightScope = $this->scope->getTightScope();
		$template->load($templateType, 'type', $this->scope->getTightScope());

		if(!(bool)$template->getId()){
			$template->setType($templateType);
			$template->setScope($tightScope);
		}
		$isUseDefault = (bool) $this->getRequest()->getParam('is_use_default', false);
		if($isUseDefault){
			$template->delete();
			try{
				$template->save();
				Mage::getSingleton( 'adminhtml/session' )->addSuccess( $this->__('Template successfully saved!') );
			}catch(Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			$this->_redirect( '*/*/index', array_merge( array( 'template' => $templateType ),
													$this->scope->getSummaryForUrl()));
			return;
		}

		$fromName = $request->getParam( 'from_name' );
		$fromEmail = $request->getParam( 'from_email' );
		$adminEmail = $request->getParam( 'admin_email');
		$subject = $request->getParam( 'subject' );
		$cc = $request->getParam( 'cc' );
		$bcc = $request->getParam( 'bcc' );
		$emailContent = $request->getParam( 'email_content' );
		$emailStyles = $request->getParam( 'email_styles' );
		$isActive = $request->getParam('active');

		if( empty($emailContent) || empty($fromName) || empty($subject) || empty($fromEmail) ){
			Mage::getSingleton('adminhtml/session')->addError( $this->__('Please fill all required fields!') );
			$this->_redirect( '*/*/index', array_merge( array( 'template' => $templateType ),
													$this->scope->getSummaryForUrl()));
			return;
		}

		$template->setFromName( $fromName );
		$template->setFromEmail( $fromEmail );
		$template->setAdminEmail($adminEmail);
		$template->setSubject( $subject );
		$template->setCc( $cc );
		$template->setBcc( $bcc );
		$template->setEmailContent( $emailContent );
		$template->setEmailStyles( $emailStyles );
		$template->setActive($isActive);

		try{
			$template->save();
			Mage::getSingleton( 'adminhtml/session' )->addSuccess( $this->__('Template successfully saved!') );
		}catch(Exception $e){
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}

		$this->_redirect( '*/*/index', array_merge( array( 'template' => $templateType ),
													$this->scope->getSummaryForUrl() ) );
	}

	public function engineAction() {
		$action = $this->getRequest()->getParam('action');

		try{
			if(in_array($action, array('activate', 'deactivate'))){
				$value = $action == 'activate' ? 1 : 0;

				/** @var $sett Itoris_PendingRegistration_Model_Settings */
				$sett = Mage::getModel('itoris_pendingregistration/settings');
				$tightScope = $this->scope->getTightScope();
				$sett->load('active', 'name', $tightScope);
				$sett->setValue($value);
				$sett->setName('active');
				$sett->setScope($tightScope);
				$sett->save();

				if ($value) {
					Mage::getSingleton( 'adminhtml/session' )->addSuccess( $this->__('Engine successfully activated!') );
				} else {
					Mage::getSingleton( 'adminhtml/session' )->addError( $this->__('Engine deactivated!') );
				}

			}else{
				throw new Exception($this->__("Invalid action."));
			}
		}catch(Exception $e){
			Mage::logException($e);
			Mage::getSingleton( 'adminhtml/session' )->addError($e->getMessage());
		}

		$backUrl = $this->getRequest()->getParam('back_url');
		$hurl = Mage::helper('core/url');
		$backUrl = $hurl->urlDecode($backUrl);
		$this->getResponse()->setRedirect($backUrl);
	}

	public function toggleEngineScopeAction(){
		$action = $this->getRequest()->getParam('action');

		try{
			if(in_array($action, array('activate', 'deactivate'))){
				$value = $action == 'activate' ? true : false;

				/** @var $sett Itoris_PendingRegistration_Model_Settings */
				$sett = Mage::getModel('itoris_pendingregistration/settings');
				$tightScope = $this->scope->getTightScope();

				if ($value) {
					$sett->load('active', 'name', $tightScope);
					$sett->delete();
				} else {
					$sett->load('active','name', $this->scope);
					$sett->unsetData('id');
					$sett->setScope($this->scope->getTightScope());
				}
				$sett->save();

			}else{
				throw new Exception($this->__("Invalid action."));
			}
		}catch(Exception $e){
			Mage::logException($e);
			Mage::getSingleton( 'adminhtml/session' )->addError($e->getMessage());
		}

		$backUrl = $this->getRequest()->getParam('back_url');
		$hurl = Mage::helper('core/url');
		$backUrl = $hurl->urlDecode($backUrl);
		$this->getResponse()->setRedirect($backUrl);
	}
	
	public function existenAction()
	{
		$this->currAction = 'existen';
		
		$this->loadLayout();
		$layout = $this->getLayout();
		
		$helper = Mage::helper( 'itoris_pendingregistration/data' );

		/** @var $button Itoris_PendingRegistration_Block_Admin_Dropdown */
		$button = $layout->createBlock( 'itoris_pendingregistration/admin_dropdown' );
		$button->setScope($this->scope);
		$layout->getBlock( 'left' )->append( $button );

		/** @var $tabs Itoris_PendingRegistration_Block_Admin_Tabs */
		$tabs = $layout->createBlock( 'itoris_pendingregistration/admin_tabs' );
		$tabs->setScope($this->scope);
		$tabs->initTabs();
		$layout->getBlock( 'left' )->append( $tabs );
		
		$existen = $layout->createBlock( 'itoris_pendingregistration/admin_existen' );
		$layout->getBlock( 'content' )->append( $existen );
		
		$this->renderLayout();
	}
	
	public function existenPostAction()
	{
		$helper = $this->getDataHelper();
		$status = $this->getRequest()->getParam( 'status' );
		$email_to_user = $this->getRequest()->getParam( 'email_to_user' );
		
		if ($status != 'pending' && $status != 'active') {
			Mage::getSingleton('adminhtml/session')->addError( $this->__('Please select value!') );
			$this->_redirect( '*/*/existen' );
			return;
		}
		
		$status = $status == 'pending' ? 0 : 1;
		
		$db = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
		$usersTableName = Mage::getSingleton( 'core/resource' )->getTableName( 'itoris_pendingregistration_users' );
		$customerTableName = Mage::getSingleton( 'core/resource' )->getTableName( 'customer_entity' );
		$result = $db->query( 'SELECT entity_id FROM '.$customerTableName );
		$customers = $result->fetchAll();
		$cCnt = count( $customers );
		for( $i=0; $i<$cCnt; $i++ )
		{
			$cid = intval( $customers[ $i ][ 'entity_id' ] );
			$result = $db->query( 'SELECT COUNT(*) FROM '.$usersTableName.' WHERE customer_id='.$cid );
			if( !$result->fetchColumn( 0 ) )
			{
				$db->query( 'INSERT INTO '.$usersTableName.' SET customer_id='.$cid.', date=CURRENT_TIMESTAMP(), status='.$status );
			}
			else
			{
				$db->query( 'UPDATE '.$usersTableName.' SET status='.$status );
			}
		}
		
		if( $status == 0 && $email_to_user )
		{
			if( $helper->isCanSendEmail( IPR_EMAIL_REG_TO_USER, $this->scope ) )
			{
				for( $i=0; $i<$cCnt; $i++ )
				{
					$user = Mage::getModel( 'customer/customer' )->load( intval( $customers[ $i ][ 'entity_id' ] ) );	
					$helper->sendEmail( IPR_EMAIL_REG_TO_USER, $user, $this->scope );
				}
				Mage::getSingleton('adminhtml/session')->addSuccess( $this->__('Emails successfully sent!') );
			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError( $this->__('Can\'t send emails. Email template not activated or not configured properly!') );
			}
		}
		
		Mage::getSingleton('adminhtml/session')->addSuccess( $this->__('Statuses successfully changed!') );
		$this->_redirect( '*/*/existen' );
	}

	public function customergroupsAction() {
		$this->loadLayout();
		$layout = $this->getLayout();

		$switcher = $this->getLayout()->createBlock('itoris_pendingregistration/admin_storeSwitcher');
		$this->getLayout()->getBlock('left')->append($switcher);

		/** @var $button Itoris_PendingRegistration_Block_Admin_Dropdown */
		$button = $layout->createBlock('itoris_pendingregistration/admin_dropdown');
		$button->setScope($this->scope);
		$layout->getBlock('left')->append($button);

		/** @var $tabs Itoris_PendingRegistration_Block_Admin_Tabs */
		$tabs = $layout->createBlock('itoris_pendingregistration/admin_tabs');
		$tabs->setScope($this->scope);
		$tabs->initTabs();
		$tabs->setActiveSection('customer_groups');
		$layout->getBlock('left')->append($tabs);

		$layout->getBlock('content')->append($layout->createBlock( 'itoris_pendingregistration/admin_customerGroups'));

		$this->renderLayout();
	}

	public function customergroupsPostAction() {
		$storeCode = $this->getRequest()->getParam('store');
		$websiteCode = $this->getRequest()->getParam('website');
		try {
			$storeId = Mage::app()->getStore($storeCode)->getId();
			$websiteId = Mage::app()->getWebsite($websiteCode)->getId();
			$customerGroups = $this->getRequest()->getPost('customer_groups');
			Mage::getModel('itoris_pendingregistration/customerGroup')->saveGroups($customerGroups, $websiteId, $storeId, $this->getRequest()->getParam('use_default_customer_group'));
			$this->_getSession()->addSuccess('Customer Groups have been saved');
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addError('Customer Groups have not been saved');
		}

		$this->_redirect('*/*/customerGroups', array('_current' => true));
	}

	/**
	 * @return Itoris_PendingRegistration_Helper_Data
	 */
	private function getDataHelper(){
		return Mage::helper('itoris_pendingregistration');
	}

	protected function _isAllowed(){
    	return Mage::getSingleton('admin/session')->isAllowed('system/itoris_extensions/itoris_pendingregistration');
    }
}?>