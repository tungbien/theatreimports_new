<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

$this->run("

create table {$this->getTable('itoris_pendingregistration_customergroups')} (
	`entry_id` int unsigned not null auto_increment primary key,
	`group_id` smallint(5) unsigned not null,
	`store_id` smallint(5) unsigned not null,
	`website_id` smallint(5) unsigned not null,
	`all_groups` bool null,
	foreign key (`group_id`) references {$this->getTable('customer_group')} (`customer_group_id`) on delete cascade on update cascade,
	foreign key (`store_id`) references {$this->getTable('core_store')} (`store_id`) on delete cascade on update cascade,
	foreign key (`website_id`) references {$this->getTable('core_website')} (`website_id`) on delete cascade on update cascade
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

");

?>