<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2014 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


Mage::helper('itoris_pendingregistration')->checkConfiguration();

$templatesTable = $this->getTable('itoris_pendingregistration_templates');
$settingsTable = $this->getTable('itoris_pendingregistration_settings');
$usersTable  = $this->getTable('itoris_pendingregistration_users');
$this->run("
ALTER TABLE {$templatesTable} CONVERT TO CHARACTER SET utf8 collate utf8_general_ci;
ALTER TABLE {$templatesTable} DEFAULT CHARACTER SET utf8;

ALTER TABLE {$settingsTable} CONVERT TO CHARACTER SET utf8 collate utf8_general_ci;
ALTER TABLE {$settingsTable} DEFAULT CHARACTER SET utf8;

ALTER TABLE {$usersTable} CONVERT TO CHARACTER SET utf8 collate utf8_general_ci;
ALTER TABLE {$usersTable} DEFAULT CHARACTER SET utf8;
");

?>