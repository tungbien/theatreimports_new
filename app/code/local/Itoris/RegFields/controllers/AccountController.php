<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';

class Itoris_RegFields_AccountController extends Mage_Customer_AccountController {

	protected $addressParams = array('company', 'telephone', 'street', 'city', 'country_id', 'region_id', 'postcode', 'fax');

	public function createAction() {
	    if ($this->_getSession()->isLoggedIn()) {
    	    $this->_redirect('*/*');
        	return;
    	}

    	$this->loadLayout();
    	$this->_initLayoutMessages('customer/session');
		if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite())) {
			/** @var $settingsModel Itoris_RegFields_Model_Settings */
			$settingsModel = $this->getDataHelper()->isEnabled();
			if ($settingsModel) {
				$registerForm = $this->getLayout()->getBlock('customer_form_register');
				/** @var $formModel Itoris_RegFields_Model_Form */
				$formModel = Mage::getModel('itoris_regfields/form');
				$websiteId = Mage::app()->getWebsite()->getId();
				$storeId = Mage::app()->getStore()->getId();
				$registerForm->setFormConfig($formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId)));
				$registerForm->setTemplate('itoris/regfields/form/register.phtml');
			}
		}
    	$this->renderLayout();
	}

 	public function createPostAction() {
    	$session = $this->_getSession();
    	if ($session->isLoggedIn()) {
        	$this->_redirect('*/*/');
         	return;
     	}
     	$session->setEscapeMessages(true); // prevent XSS injection in user input
     	if ($this->getRequest()->isPost()) {
        	$errors = array();
			 $redirectPath = $this->prepareRedirectPath();
			 /** @var $customer Mage_Customer_Model_Customer */
         	if (!$customer = Mage::registry('current_customer')) {
            	$customer = Mage::getModel('customer/customer')->setId(null);
         	}

			 $settingsModel = $this->getDataHelper()->isEnabled();
			 if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite()) && $settingsModel) {
				 $params = $this->getRequest()->getParam('itoris', array());
				 $customer->addData($params);
				 $customParams = $params;
				 $customParams['firstname'] = $this->getRequest()->getParam('firstname');
				 $customParams['lastname'] = $this->getRequest()->getParam('lastname');
				 $customParams['email'] = $this->getRequest()->getParam('email');
				 $customParams['is_subscribed'] = $this->getRequest()->getParam('is_subscribed');
				 $session->setCustomOptions($customParams);
				 $captcha = $this->getRequest()->getParam('captcha');
				 if ($captcha) {
					foreach ($captcha as $key => $value) {
						$captchaType = explode('_', $key);
						/** @var $captchaHelper Itoris_RegFields_Helper_Captcha */
						$captchaHelper = Mage::helper('itoris_regfields/captcha');
						if(!$captchaHelper->captchaValidate($value, $captchaType[0])) {
							$errors[] = $this->__('Wrong captcha code!');
						}
					}
				 }
				 if ($_FILES && isset($_FILES['itoris'])) {
					 $fileItoris = $_FILES['itoris'];
					 foreach($fileItoris['name'] as $key => $fileName) {
						 if ($fileItoris['size'][$key]) {
							$file = $this->getDataHelper()->uploadFiles('itoris['.$key.']');
							 if ($file['error']) {
								 $errors[] = $file['error'];
							 } else {
								$params[$key] = serialize(array(
									'name' => $fileName,
									'file' => $file['file'],
									'size' => $fileItoris['size'][$key],
									'mime' => $fileItoris['type'][$key],
								));
							 }
						 }
					 }
				 }
				 /** @var $formModel Itoris_RegFields_Model_Form */
				 $formModel = Mage::getModel('itoris_regfields/form');
				 $websiteId = Mage::app()->getWebsite()->getId();
				 $storeId = Mage::app()->getStore()->getId();
				 $formConfig = $formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId));
				 $formValidation = Mage::helper('itoris_regfields/field')->validate($params, $formConfig, true);
				 $errors = array_merge($formValidation, $errors);
			 }

			 $hasFormClass = file_exists(Mage::getBaseDir().DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Customer'.DS.'Model'.DS.'Form.php');
			 if ($hasFormClass) {
	         	/* @var $customerForm Mage_Customer_Model_Form */
    	     	$customerForm = Mage::getModel('customer/form');
        	 	$customerForm->setFormCode('customer_account_create')
            		 ->setEntity($customer);

         		$customerData = $customerForm->extractData($this->getRequest());
				 if (isset($params)) {
					 $customerData = array_merge($customerData, $params);
				 }

				 $customerData = $this->_filterPostData($customerData);

			 } else {
				 $data = $this->getRequest()->getPost();
				 if (isset($params)) {
					 $data = array_merge($data, $params);
				 }
				 $data = $this->_filterPostData($data);
				 foreach (Mage::getConfig()->getFieldset('customer_account') as $code=>$node) {
					if ($node->is('create') && isset($data[$code])) {
						if ($code == 'email') {
							$data[$code] = trim($data[$code]);
						}
						$customer->setData($code, $data[$code]);
					}
				}
			 }

         	if ($this->getRequest()->getParam('is_subscribed', false)) {
            	$customer->setIsSubscribed(1);
         	}

         	/**
          	 * Initialize customer group id
          	 */
         	$customer->getGroupId();

			if ($this->getRequest()->getPost('create_address')) {
				$addressData = $this->getRequest()->getPost();
				if (isset($params)) {
					$addressData = array_merge($addressData, $params);
				}
				if ($this->getRequest()->getParam('default_billing', false)) {
					if ($this->_canCreateAddress($addressData)) {
						$billinAddress = Mage::getModel('customer/address')
							->setData($addressData)
							->setIsDefaultBilling(true)
							->setId(null);
						$customer->addAddress($billinAddress);
					//	$addressErrors = $billinAddress->validate();
					//	if (is_array($addressErrors)) {
					//		$errors = array_merge($addressErrors, $errors);
					//	}
					}
				}
				if ($this->getRequest()->getParam('default_shipping', false)) {
					$shippingAddressData = array();
					foreach ($params as $key => $value) {
						if (strpos($key, 's_') === 0) {
							$shippingAddressData[substr($key, 2)] = $value;
						}
					}
					$origShippingData = array();
					if ($this->_canCreateAddress($shippingAddressData)) {
						$addressData = $this->getRequest()->getPost();
						$shippingAddressData = array_merge($addressData, $params, $shippingAddressData);
						foreach ($this->addressParams as $addressParam) {
							if (isset($shippingAddressData[$addressParam]) && !isset($origShippingData[$addressParam])) {
								unset($shippingAddressData[$addressParam]);
							}
						}
						$shippingAddress = Mage::getModel('customer/address')
							->setData($shippingAddressData)
							->setIsDefaultShipping(true)
							->setId(null);
						$customer->addAddress($shippingAddress);
	//					$addressErrors = $shippingAddress->validate();
	//					if (is_array($addressErrors)) {
	//						$errors = array_merge($addressErrors, $errors);
	//					}
					}
				}
			}

         	try {
				 $customerErrors = true;
				 if ($hasFormClass) {
	            	$customerErrors = $customerForm->validateData($customerData);
				 }
             	if ($customerErrors !== true) {
                	$errors = array_merge($customerErrors, $errors);
             	} else {
					 if ($hasFormClass) {
	                	$customerForm->compactData($customerData);
    	             	$customer->setPassword($this->getRequest()->getPost('password'));
        	         	$customer->setConfirmation($this->getRequest()->getPost('confirmation'));
						$customer->setPasswordConfirmation($this->getRequest()->getPost('confirmation'));
					 }
            	   	$customerErrors = $customer->validate();
                 	if (is_array($customerErrors)) {
                    	$errors = array_merge($customerErrors, $errors);
                 	}
             	}

             	$validationResult = count($errors) == 0;

             	if (true === $validationResult) {
                	$customer->save();

					 if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite()) && $settingsModel) {
						 /** @var $customerOptions Itoris_RegFields_Model_Customer */
						 $customerOptions = Mage::getModel('itoris_regfields/customer');
						 $customerOptions->saveOptions($params, $customer->getId());
					 }

					 $session->unsetData('custom_options');

                 	Mage::dispatchEvent('customer_register_success',
                    	 array('account_controller' => $this, 'customer' => $customer)
                 	);

					 $session->setCustomerFormData($this->getRequest()->getPost());

					 if ($customer->getPendingMessage() && ($this instanceof Itoris_StoreLoginControl_CustomRegistrationController || $this instanceof Itoris_SmartLogin_CustomRegistrationController)) {
						 return $customer;
					 } else {
						if ($customer->isConfirmationRequired()) {
							 $customer->sendNewAccountEmail(
								 'confirmation',
								$session->getBeforeAuthUrl(),
								Mage::app()->getStore()->getId()
							);
							$session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
							if ($redirectPath) {
								$session->setAfterItorisRegForm(true);
								$this->_redirect($redirectPath);
							} else {
								$this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure'=>true)));
							}
							return;
						} else {
							$session->setCustomerAsLoggedIn($customer);
							$session->setCustomerIsLoggedIn(true);
							$url = $this->_welcomeCustomer($customer);
							if ($redirectPath) {
								$session->setAfterItorisRegForm(true);
								$this->_redirect($redirectPath);
							} else {
								$this->_redirectSuccess($url);
							}
							return;
						}
					 }
             	} else {
                	$session->setCustomerFormData($this->getRequest()->getPost());
                 	if (is_array($errors)) {
                    	foreach ($errors as $errorMessage) {
                        	$session->addError($errorMessage);
                     	}
                 	} else {
                    	$session->addError($this->__('Invalid customer data'));
                 	}
             	}
         	} catch (Mage_Core_Exception $e) {
            	$session->setCustomerFormData($this->getRequest()->getPost());
             	if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                	$url = Mage::getUrl('customer/account/forgotpassword');
                 	$message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                 	$session->setEscapeMessages(false);
             	} else {
                	$message = $e->getMessage();
             	}
             	$session->addError($message);
         	} catch (Exception $e) {
				 $session->addError($e->getMessage());
            	$session->setCustomerFormData($this->getRequest()->getPost())
                	 ->addException($e, $this->__('Cannot save the customer.'));
         	}
     	}
		 if ($redirectPath) {
			 $session->setAfterItorisRegForm(true);
			 $this->_redirect($redirectPath);
		 } else {
			 $this->_redirectError(Mage::getUrl('*/*/create', array('_secure' => true)));
		 }
 	}

	protected function _canCreateAddress($data) {
		foreach ($this->addressParams as $param) {
			if (isset($data[$param]) && !empty($data[$param])) {
				if (is_array($data[$param])) {
					foreach ($data[$param] as $value) {
						if (!empty($value)) {
							return true;
						}
					}
					continue;
				}
				return true;
			}
		}
		return false;
	}

	public function editAction() {
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$block = $this->getLayout()->getBlock('customer_edit');
		if ($block) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$data = $this->_getSession()->getCustomerFormData(true);
		$customer = $this->_getSession()->getCustomer();
		if (!empty($data)) {
			$customer->addData($data);
		}
		if ($this->getRequest()->getParam('changepass')==1){
			$customer->setChangePassword(1);
		}

		if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite())) {
					$store = $customer->getStore();
					$websiteId = $store->getWebsiteId();
					$storeId = $store->getId();
					/** @var $settingsModel Itoris_RegFields_Model_Settings */
					$settingsModel = $this->getDataHelper()->isEnabled($websiteId, $storeId);
					if ($settingsModel) {
						$editBlock = $this->getLayout()->getBlock('customer_edit');
						$editBlock->setTemplate('itoris/regfields/edit.phtml');
						/** @var $formModel Itoris_RegFields_Model_Form */
						$formModel = Mage::getModel('itoris_regfields/form');
						$editBlock->setFormConfig($formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId)));
					}
		}

		$this->getLayout()->getBlock('head')->setTitle($this->__('Account Information'));
		$this->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
		$this->renderLayout();
 	}

	public function editPostAction() {
		if (!$this->_validateFormKey()) {
			return $this->_redirect('*/*/edit');
		}

		$errors = array();

		if ($this->getRequest()->isPost()) {
			/** @var $customer Mage_Customer_Model_Customer */
			$customer = $this->_getSession()->getCustomer();
			$store = $customer->getStore();
			$websiteId = $store->getWebsiteId();
			$storeId = $store->getId();
			/** @var $settingsModel Itoris_RegFields_Model_Settings */
			$settingsModel = $this->getDataHelper()->isEnabled($websiteId, $storeId);
			if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite()) && $settingsModel) {
				$params = $this->getRequest()->getParam('itoris', array());
				if ($_FILES && isset($_FILES['itoris'])) {
					$fileItoris = $_FILES['itoris'];
					foreach($fileItoris['name'] as $key => $fileName) {
						if ($fileItoris['size'][$key]) {
							$file = $this->getDataHelper()->uploadFiles('itoris['.$key.']');
							if ($file['error']) {
								$errors[] = $file['error'];
							} else {
								$params[$key] = serialize(array(
									'name' => $fileName,
									'file' => $file['file'],
									'size' => $fileItoris['size'][$key],
									'mime' => $fileItoris['type'][$key],
								));
							}
						}
					}
				}
				/** @var $formModel Itoris_RegFields_Model_Form */
				$formModel = Mage::getModel('itoris_regfields/form');
				$formConfig = $formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId));
				$formValidation = Mage::helper('itoris_regfields/field')->validate($params, $formConfig, true);
				$errors = array_merge($formValidation, $errors);
			}

			$hasFormClass = file_exists(Mage::getBaseDir().DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Customer'.DS.'Model'.DS.'Form.php');
			if ($hasFormClass) {
				/** @var $customerForm Mage_Customer_Model_Form */
				$customerForm = Mage::getModel('customer/form');
				$customerForm->setFormCode('customer_account_edit')
					->setEntity($customer);

				$customerData = $customerForm->extractData($this->getRequest());


			} else {
				$fields = Mage::getConfig()->getFieldset('customer_account');
				$data = $this->_filterPostData($this->getRequest()->getPost());

				foreach ($fields as $code=>$node) {
					if ($node->is('update') && isset($data[$code])) {
						$customer->setData($code, $data[$code]);
					}
				}

				$validateErrors = $customer->validate();
				$errors = array_merge($validateErrors, $errors);
			}
			$customerErrors = true;
			if ($hasFormClass) {
				$customerErrors = $customerForm->validateData($customerData);
			}
			if ($customerErrors !== true) {
				$errors = array_merge($customerErrors, $errors);
			} else {
				if ($hasFormClass) {
					$customerForm->compactData($customerData);
				}

				// If password change was requested then add it to common validation scheme
				if ($this->getRequest()->getParam('change_password')) {
					$currPass   = $this->getRequest()->getPost('current_password');
					$newPass    = $this->getRequest()->getPost('password');
					$confPass   = $this->getRequest()->getPost('confirmation');

					$oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
					if (Mage::helper('core/string')->strpos($oldPass, ':')) {
						list($_salt, $salt) = explode(':', $oldPass);
					} else {
						$salt = false;
					}

					if ($customer->hashPassword($currPass, $salt) == $oldPass) {
						if (strlen($newPass)) {
							/**
							 * Set entered password and its confirmation - they
							 * will be validated later to match each other and be of right length
							 */
							$customer->setPassword($newPass);
							$customer->setConfirmation($confPass);
							$customer->setPasswordConfirmation($confPass);
						} else {
							$errors[] = $this->__('New password field cannot be empty.');
						}
					} else {
						$errors[] = $this->__('Invalid current password');
					}
				}

				// Validate account and compose list of errors if any
				$customerErrors = $customer->validate();
				if (is_array($customerErrors)) {
					$errors = array_merge($errors, $customerErrors);
				}
			}

			if (!empty($errors)) {
				$this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
				foreach ($errors as $message) {
					$this->_getSession()->addError($message);
				}
				$this->_redirect('*/*/edit');
				return $this;
			}

			try {
				$customer->setConfirmation(null);
				$customer->setPasswordConfirmation(null);
				$customer->save();
				$this->_getSession()->setCustomer($customer)
					->addSuccess($this->__('The account information has been saved.'));

				if ($this->getDataHelper()->isRegisteredAutonomous(Mage::app()->getWebsite()) && $this->getDataHelper()->isEnabled()) {
					/** @var $customerOptions Itoris_RegFields_Model_Customer */
					$customerOptions = Mage::getModel('itoris_regfields/customer');
					$customerOptions->saveOptions($params, $customer->getId());
				}
//
//				/** @var $customerOptions Itoris_RegFields_Model_Customer */
//				$customerOptions = Mage::getModel('itoris_regfields/customer');
//				$customerOptions->saveOptions($this->getRequest()->getParams(), $customer->getId());

				$this->_redirect('customer/account');
				return;
			} catch (Mage_Core_Exception $e) {
				$this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
					->addError($e->getMessage());
			} catch (Exception $e) {
				$this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
					->addException($e, $this->__('Cannot save the customer.'));
			}
		}

		$this->_redirect('*/*/edit');
	}

	public function confirmationAction() {
		$email = $this->getRequest()->getPost('email');
		if (!$this->getDataHelper()->isEnabled() || !$email) {
			return parent::confirmationAction();
		}

		$customer = Mage::getModel('customer/customer');
		if ($this->_getSession()->isLoggedIn()) {
			$this->_redirect('*/*/');
			return;
		}
		if ($email) {
			try {
				$customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
				if (!$customer->getId()) {
					throw new Exception('');
				}
				if ($customer->getConfirmation()) {
					Mage::getModel('itoris_regfields/customer')->addOptionsToCustomer($customer);
					$customer->sendNewAccountEmail('confirmation', '', Mage::app()->getStore()->getId());
					$this->_getSession()->addSuccess($this->__('Please, check your email for confirmation key.'));
				} else {
					$this->_getSession()->addSuccess($this->__('This email does not require confirmation.'));
				}
				$this->_getSession()->setUsername($email);
				$this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure' => true)));
			}
			catch (Exception $e) {
				$this->_getSession()->addError($this->__('Wrong email.'));
				$this->_redirectError(Mage::getUrl('*/*/*', array('email' => $email, '_secure' => true)));
			}
			return;
		}

		// output form
		$this->loadLayout();

		$this->getLayout()->getBlock('accountConfirmation')
			->setEmail($this->getRequest()->getParam('email', $email));

		$this->_initLayoutMessages('customer/session');
		$this->renderLayout();
	}

	protected function prepareRedirectPath() {
		switch ($this->getRequest()->getParam('redirect_after')) {
			case 'checkout':
				return 'checkout/onepage';
			default:
				return null;
		}
	}

	/**
	 * @return Itoris_RegFields_Helper_Data
	 */
	private function getDataHelper() {
		return Mage::helper('itoris_regfields');
	}

}
?>