<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Model_Form extends Mage_Core_Model_Abstract {

	private $defaultSections = null;

	public function __construct() {
		$this->_init('itoris_regfields/form');
	}

	public function getSectionsJson($viewId) {
		$config = $this->getFormConfig($viewId);
		return Zend_Json::encode($config);
	}

	public function getFormConfig($viewId) {
		$this->load($viewId, 'view_id');
		$config = $this->getConfig();
		if ($config) {
			$config = unserialize($config);
			$config = array_values($config);
			for ($i = 0; $i < count($config); $i++) {
				if (isset($config[$i]['fields'])) {
					$config[$i]['fields'] = array_values($config[$i]['fields']);
					for ($j = 0; $j < count($config[$i]['fields']); $j++) {
						if (isset($config[$i]['fields'][$j]['items'])) {
							$config[$i]['fields'][$j]['items'] = array_values($config[$i]['fields'][$j]['items']);
						}
					}
				}
			}
		} else {
			$config = $this->getFieldHelper()->getDefaultSections();
		}
		return $config;
	}

	public function getDefaultSectionsJson() {
		$config = $this->getFieldHelper()->getDefaultSections();
		return Zend_Json::encode($config);
	}

	/**
	 * @return Itoris_RegFields_Helper_Field
	 */
	private function getFieldHelper() {
		return Mage::helper('itoris_regfields/field');
	}

}
?>