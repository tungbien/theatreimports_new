if (!Itoris) {
	var Itoris = {};
}

Itoris.ProductVisibility = Class.create({
	initialize : function() {
		var curObj = this;
		var productTimer  = null;
		productTimer = new PeriodicalExecuter(function() {
			curObj.product(productTimer);
		}, 0.2);
	},
	product : function(t) {
		if ($$('.itoris_product_visibility')[0]) {
			t.stop();
		}
        $$('.availability.out-of-stock').each(function(item){item.style.visibility="visible";});
		for (var i = 0; i < $$('.itoris_product_visibility').length; i++) {
            if ($('product_addtocart_form')) {
                var parentDiv = $$('.itoris_product_visibility')[i].up();
            } else {
                var parentDiv = $$('.itoris_product_visibility')[i].up('li');
            }
            /*if (!parentDiv.hasClassName('product-shop')) {
                parentDiv = parentDiv.up();
            }*/
            parentDiv.select('.availability.out-of-stock').each(function(item){item.remove();});
			if (parentDiv.select('.availability.in-stock')[0]) {
				parentDiv.select('.availability.in-stock')[0].remove();
			}
			if (parentDiv.select('.button.btn-cart')[0]) {
				parentDiv.select('.button.btn-cart')[0].remove();
			}
            if (parentDiv.select('.qty')[0]) {
                parentDiv.select('.qty')[0].remove();
            }
            if (parentDiv.select('label')[0]) {
                parentDiv.select('label')[0].remove();
            }
            if (parentDiv.select('.add-to-box')[0] && parentDiv.select('.add-to-box')[0].select('.or')[0]) {
                parentDiv.select('.add-to-box')[0].select('.or')[0].remove();
			}
		}
		for (var i = 0; i < $$('.itoris_product_visibility_restriction_message').length; i++) {
			var parentDiv = $$('.itoris_product_visibility_restriction_message')[i].up();
			if (parentDiv.hasClassName('price-box-bundle') && parentDiv.select('.itoris_product_visibility_restriction_message')[0]) {
				parentDiv.select('.itoris_product_visibility_restriction_message')[0].remove();
			}
		}
		if ($$('.product-shop')[0]) {
			if ($$('.product-shop')[0].select('.itoris_product_visibility').length > 1 && !$$('.product-shop')[0].select('.itoris_product_visibility_restriction_message')[0]) {
				for (var i = 0; i < $$('.product-shop')[0].select('.itoris_product_visibility').length; i++) {
					if (i != 0) {
						$$('.product-shop')[0].select('.itoris_product_visibility')[i].remove();
					}
				}
			}
			if ($$('.product-shop')[0].select('.itoris_product_visibility_restriction_message')[0] && $$('.product-shop')[0].select('.itoris_product_visibility_restriction_message').length > 1) {
				$$('.product-shop')[0].select('.itoris_product_visibility_restriction_message')[0].remove();
			}
		}
	}
});
var itorisProductVisibility = new Itoris.ProductVisibility();